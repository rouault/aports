# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=felix
pkgver=2.10.1
pkgrel=0
pkgdesc="Simple TUI file manager with vim-like key mapping"
url="https://kyoheiu.dev/felix/"
# s390x: nix crate doesn't build yet
arch="all !s390x"
license="MIT"
makedepends="
	bzip2-dev
	cargo
	cargo-auditable
	libgit2-dev
	oniguruma-dev
	zlib-ng-dev
	zstd-dev
	"
checkdepends="zoxide"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/kyoheiu/felix/archive/refs/tags/v$pkgver.tar.gz
	use-system-libs.patch
	"

export RUSTONIG_DYNAMIC_LIBONIG=1

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
		z-ng = { rustc-link-lib = ["z-ng"], rustc-cfg = ["zng"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/fx -t "$pkgdir"/usr/bin/
}

sha512sums="
605a767683444a671bb78381f3dc59ed496465dfe3cf61e4fbe25be38ddfa6de895345a0501ff5507d0f83405fae0de4b562102d2b1fbd34d95f7bb3bd14d315  felix-2.10.1.tar.gz
39a1c327594c710af95c420ee2e835a0db54612c0ec89cc639e77c540e26dbd414e289a712562d58ba609ca8dbf5c95e087f5e691b9164909123f100127b1c93  use-system-libs.patch
"
